const mongoose = require('mongoose');

const { Schema } = mongoose;
const LeaveSchema = new Schema({
    name: String,
    employeeID: String,
    leaveType:String,
    dateStart:String,
    dateEnd:String,
    reason:String,
})

module.exports = mongoose.model("Leave",LeaveSchema);