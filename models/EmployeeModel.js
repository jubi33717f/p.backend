
const mongoose = require('mongoose');

const { Schema } = mongoose;

const EmployeeSchema = new Schema({
    account: String,//email
    password: {type:String, default:'000000'},
    firstname:String,
    lastname:String,      
    gender:String,//要改
    address:Object,//autoinput
    mobile:String,
    startDate:String,//返回的是string
    taxFileNumber:String,
    employeeID: String,
    photo: String,
    role: String,
    department: String,
    salary: Number,
    bonus:Number,
    earnings:Number,
    tax:Number,
    unpaidLeave:Number,
    deduction:Number,
    punchIn:Number,
    punchOut:Number,
    leaveRecord: Array,
    leaveNumber: {type:Number,default:0},
    leaveRequest:Object,//object
    messageReadTime:{type:String,default:'0'},
})

module.exports = mongoose.model("Employee",EmployeeSchema);