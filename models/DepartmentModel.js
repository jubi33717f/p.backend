
const mongoose = require('mongoose');

const { Schema } = mongoose;

const DepartmentSchema = new Schema({
    departmentID: Number,
    departmentName: String,
    departmentHead: String,
    departmentHeadID: String,
    employeeAmount: Number,
})

module.exports = mongoose.model("Department",DepartmentSchema);