const mongoose = require('mongoose');

const { Schema } = mongoose;
const MessageSchema = new Schema({
    name: String,
    photo:String,
    writerId:String,
    writerRole:String,
    postTime: String,
    tag:String,
    content:String,
    img:String,
    comments:Array,
    like:Number,
    likePeople:Array,
    dislike:Number,
    dislikePeople:Array
})

module.exports = mongoose.model("Message",MessageSchema);