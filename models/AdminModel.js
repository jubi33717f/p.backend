/**
 * Admin
 * {
 *      account:String,    
 *      password:String,
 *      experience:number, //optional
 *      employees:number,
 *      clients:number  //optional
 * }
 */
const mongoose = require('mongoose');

const { Schema } = mongoose;

const AdminSchema = new Schema({
    account: String,
    password: {type:String, default:'000000'},
    firstname:String,
    lastname:String,      
    gender:String,//要改
    photo: String,
    experience: Number,
    employee: Number,
    clients: Number,
    messageReadTime:{type:String, default:'0'},
})

module.exports = mongoose.model("Admin",AdminSchema);