'use strict'

const config = require('./config');
const Router = require('koa-router');

const mongoose = require('mongoose');
const router = new Router();
const createError =require('http-errors');
const { HttpError } =require('koa');
const authControllers = require('./controllers/auth')
const employeeControllers = require('./controllers/employee')
const adminControllers = require('./controllers/admin')
const departmentControllers = require('./controllers/department')
const dashboardControllers = require('./controllers/dashboard')
const messageControllers = require('./controllers/message')
const leaveControllers = require('./controllers/leave')
//token&hashpassword
const jwt = require('jsonwebtoken');
const bcrypt = require('bcryptjs');


//employee&admin一起=>post[login]✅

// router.get("/l", async (ctx) => {
//     ctx.body = "Hello";
// });
router.post("/login", authControllers.login);
router.put('/change-password',authControllers.changepassword);
//根据account/password查找employee
//employee=>post[add one employee]✅
//employee=>get[拿所有employee信息，分页]✅
//employee=>get[拿单独employee信息]✅
//employee=>put[修改信息]✅
//employee=>put[修改密码]✅舍弃
//employee=>delete[delete employee]✅没测
router.post('/new-employee',employeeControllers.store);
router.get('/employee/:page/:pageSize',employeeControllers.index);
router.get("/employee/:page/:pageSize/:field",employeeControllers.indexSort)
router.get('/employeename/:name',employeeControllers.indexEmployee);
router.get('/employee/:id',employeeControllers.show)
router.put('/employee/:id',employeeControllers.update)
router.delete('/employee/:id',employeeControllers.destroy)

//admin=>post[add one admin]✅
//admin=>get[拿所有admin信息，分页]✅
//admin=>get[拿某admin信息]✅
//admin=>put[修改信息]可以不写✅
//admin=>put[修改密码]✅舍弃
//admin=>delete[delete one admin]可以不写✅没测
router.post('/new-admin',adminControllers.store)
router.get('/admin/:page/:pageSize',adminControllers.index);
router.get('/admin/:id',adminControllers.show)
router.put('/admin/:id',adminControllers.update)
router.delete('/admin/:id',adminControllers.destroy)

//department=>post[add one department] done
//department=>get[拿所有department信息，分页] done
//department=>put[修改信息] done
//department=>delete[delete one department] done
//department=>get[拿所有department信息]   Jubi(added by 6.2)
router.post("/department",departmentControllers.store)
router.get("/department/:page/:pageSize/:field",departmentControllers.indexPart)
router.delete("/department/:id",departmentControllers.destroy)
router.put("/department/:id",departmentControllers.update)
router.get("/department/all",departmentControllers.index)

//dashboard=>attendance=>get[拿所有员工出勤数量=》punchOn不为0的数据]
//dashboard=>timeout=>get（可以用总数-attendance可以不写）
//dashboard=>new employee=>(1.确定一个startdate格式new employee也需要相同格式2.完善database数据)
//dashboard=>department head=>get
router.get('/dashboard/attendance',dashboardControllers.indexAttendance)
router.get('/dashboard/newEmployee',dashboardControllers.indexNewEmployee)
router.get('/dashboard/departmentHeadInfo',dashboardControllers.indexDepartmentHeadInfo)

//store=>post增加一条message
//destory=>delete(writerId 匹配 or writerRole='Admin' )
//update=>put(writerId 匹配 or writerRole='Admin' )
//index=>get（分页，前端计算处理实现分段效果）
//indexMe=>get one(详细的post页面)
//indexReverse=>逆序排序显示=>依然需要分页+
//indexPopular=>like排序显示=>依然需要分页+
//searchContent=>关键字查询+
//indexTopic=>topic筛选=>依然需要分页+
//updateLike =>put
//updateDislike =>put
router.post('/message',messageControllers.store)
router.delete('/message/:id',messageControllers.destroy)
router.put('/message/:id',messageControllers.update)
router.put('/messageComment/:id',messageControllers.updateComment)
router.get('/message/:page/:pageSize',messageControllers.index)
router.get('/message/:id',messageControllers.indexMe)
router.get('/messageReverse/:page/:pageSize',messageControllers.indexReverse)
router.get('/messagePopular/:page/:pageSize',messageControllers.indexPopular)
router.get('/messageSearch/:searchContent',messageControllers.searchContent)
router.get('/messageTopic/:page/:pageSize/:topic',messageControllers.indexTopic)
router.put('/messageLike/:id',messageControllers.updateLike)
router.put('/messageDislike/:id',messageControllers.updateDislike)
router.get('/messageUnreadAmount/:id/:role',messageControllers.indexUnreadMessage)
router.get('/messageMyPosts/:id',messageControllers.indexMyPosts)


router.get('/leave/:page/:pageSize',leaveControllers.index);
router.get('/leave/:name',leaveControllers.indexLeave);
router.post('/leave',leaveControllers.store);
router.delete("/leave/:id/:dateStart/:dateEnd/:leaveType/:flag",leaveControllers.destroy)

//payroll=>put[更新收入] done
router.put('/payroll/update',employeeControllers.salaryUpdate)

module.exports = router;

