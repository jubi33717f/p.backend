"use strict";

const Koa = require("koa");
const { oas } = require("koa-oas3");
const path = require("path");
const bodyParser = require("koa-bodyparser");
// const cors = require("@koa/cors");
const cors = require("koa-cors");
// var connectTimeout = require('connect-timeout');
// var longTimeout = connectTimeout({ time: 600000 });
//const static = require("koa-static");



const mongoose = require("mongoose");
const DATABASE_URL = process.env.DATABASE_URL?process.env.DATABASE_URL:'mongodb+srv://jrpro3:404notfound@cluster0-7nwhz.mongodb.net/test?retryWrites=true&w=majority';
mongoose.connect(DATABASE_URL).then(() => console.log('MongoDB Connected'))
.catch((err) => console.log(err));



const router = require("./router.js");
const app = new Koa();

app.use(
  oas({
    endpoint: "/openapi.json",
    uiEndpoint: "/oas",
    file: path.resolve(process.cwd(), "openapi.yaml"), //
    validatePaths: ["/else"], //
  })
);




// app.use(static(path.join(__dirname, staticPath)));
// async function permissionCheck(ctx, next) {
//     if (true) {
//       return next();
//     }
  
//     ctx.body = {
//       message: "No permisson",
//     };
//     ctx.status = 403;
// }
  
// app.use(permissionCheck);

app.use(cors()); //需要在静态资源托管的代码前面
app.use(bodyParser());

app.use(router.routes())
//.use(router.allowedMethods());
// app.use(longTimeout);
app.listen(process.env.PORT||5001);
