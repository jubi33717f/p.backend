const Department = require('../models/DepartmentModel');
const assert = require('assert');
const mongoose = require('mongoose');
//const config = require('../config');


exports.store = async (ctx)=>{
    // const token = ctx.request.headers['x-access-token'];
    // if (!token) return res.status(401).send({ auth: false, message: 'No token provided.' });
    try{
        const {body} = ctx.request.body;
        assert(body != "","Request body must exist");
        assert(body.departmentName !== undefined,"Department name must exist");
        assert(body.departmentHead !== undefined,"Department head must exist");
        assert(body.employeeAmount !== undefined,"Employee amount must exist");
        body.departmentHeadID = "123";
        const maxQuery = await Department.find().sort([["departmentID", -1]]).limit(1);
        // console.log(maxQuery);
        if (maxQuery[0]){
            body.departmentID = (maxQuery[0].departmentID+1);
        }else{
            body.departmentID = 0;
        }

        const newDepart = new Department(body);
        const res = await newDepart.save();
        ctx.body={
            message:"Created",
            id:res._id
        }
        ctx.status = 201;
    }catch(e){
        if(e instanceof assert.AssertionError){
            ctx.body = e.message;
            ctx.status = 400;
        }  
    }
}
exports.indexPart=async ctx=>{
    // const token = ctx.request.headers['x-access-token'];
    // if (!token) return res.status(401).send({ auth: false, message: 'No token provided.' });
    try{
        const {page,pageSize,field} = ctx.params;
        assert(page,"page must exist");
        assert(pageSize,"pageSize must exist");
        assert(field,"field must exist");
        const attrArray = ["departmentID","departmentHead","departmentName","employeeAmount"];
        assert(attrArray.includes(field),"field should match the attribute");
        const pageNum = parseInt(page);
        const pageSizeNum = parseInt(pageSize);
        
        const skipNum = (pageNum-1)*pageSizeNum;
        const totalNum = await Department.find().countDocuments();
        const result = await Department.find().sort([[field, 1]]).skip(skipNum).limit(pageSizeNum);
        ctx.body = {
            result:result,
            total:totalNum
        }
        ctx.status = 200;
    }catch(e){
        if(e instanceof assert.AssertionError){
            ctx.body = e.message;
            ctx.status = 400;
        }
    }
}
exports.destroy =async ctx=>{
    // const token = ctx.request.headers['x-access-token'];
    // if (!token) return res.status(401).send({ auth: false, message: 'No token provided.' });
    try{
        const {id} = ctx.params;
        assert(id,"id must exist");
        const res = await Department.deleteOne({
            _id : new mongoose.Types.ObjectId(id)
        });
        ctx.body = res;
        ctx.status = 200;
    }catch(e){
        if(e instanceof assert.AssertionError){
            ctx.body = e.message;
            ctx.status = 400;
        }
    }
}
exports.update = async (ctx)=>{
    // const token = ctx.request.headers['x-access-token'];
    // if (!token) return res.status(401).send({ auth: false, message: 'No token provided.' });
    try{
        const {id} = ctx.params;
        const {body} = ctx.request.body;
        assert(body != "","Request body must exist");
        assert(body.departmentName !== undefined,"Department name must exist");
        assert(body.departmentHead !== undefined,"Department head must exist");
        assert(body.employeeAmount !== undefined,"Employee amount must exist");
        
        const depart = await Department.findOne({
            _id : new mongoose.Types.ObjectId(id)
        });
        if (depart){
            Object.assign(depart,body);
            const newDepart = new Department(depart);
            const res = await newDepart.save();
            ctx.body = { message: "updated"};
            ctx.status = 200
        }else{
            ctx.body={
                message: `${id} not found`
            }
            ctx.status = 404;
        }
    }catch(e){
        if(e instanceof assert.AssertionError){
            ctx.body = e.message;
            ctx.status = 400;
        }  
    }
}
exports.index = async ctx=>{

    
    try{      
        const token = ctx.request.headers['x-access-token'];
        if (!token){
            ctx.body={
                auth: false, message: 'No token provided.' 
            }
            ctx.status = 401;
            return
        } 
        //const attrArray = ["departmentID","departmentHead","departmentName","employeeAmount"];    
        const result = await Department.find();  
        const totalNum = await Department.find().countDocuments();

        ctx.body = {
            result:result,
            total:totalNum
        }
        ctx.status = 200;
    }catch(e){
        if(e instanceof assert.AssertionError){
            ctx.body = e.message;
            ctx.status = 400;
        }
    }
}