const Employee = require('../models/EmployeeModel');
const mongoose = require('mongoose');
//const Department = require('../models/DepartmentModel');
// const jwt = require('jsonwebtoken');
// const bcrypt = require('bcryptjs');
const assert = require('assert');

exports.indexAttendance = async (ctx)=>{
    try{      
        // const token = ctx.request.headers['x-access-token'];
        // if (!token){
        //     ctx.body={
        //         auth: false, message: 'No token provided.' 
        //     }
        //     ctx.status = 401;
        //     return
        // }     
        const result = await Employee.find({punchIn:{$gt:0}}).countDocuments(); 

        ctx.body = {
            amount:result,
        }
        ctx.status = 200;
    }catch(e){
        if(e instanceof assert.AssertionError){
            ctx.body = e.message;
            ctx.status = 400;
        }
    }
}
exports.indexNewEmployee = async (ctx)=>{
    //24 Jun,2015

    let month='';
    const year=new Date().getFullYear()+''
    switch(new Date().getMonth()){
        case 0:{month='Jan';break;}
        case 1:{month='Feb';break;}
        case 2:{month='Mar';break;}
        case 3:{month='Apr';break;}
        case 4:{month='May';break;}
        case 5:{month='Jun';break;}
        case 6:{month='Jul';break;}
        case 7:{month='Aug';break;}
        case 8:{month='Sep';break;}
        case 9:{month='Oct';break;}
        case 10:{month='Nov';break;}
        default:month='Dec'
    }
    try{
        
        const result = await Employee.find({startDate:{$regex:new RegExp(month+','+year)}}).countDocuments(); 

        ctx.body = {
            amount:result,
        }
        ctx.status = 200;
    }catch(e){
        if(e instanceof assert.AssertionError){
            ctx.body = e.message;
            ctx.status = 400;
        }
    }  
}
exports.indexDepartmentHeadInfo = async ctx=>{
    try{
        const result = await Employee.find({role:"head"})
        ctx.body = {
            result:result,
        }
        ctx.status = 200;
    }catch(e){
        if(e instanceof assert.AssertionError){
            ctx.body = e.message;
            ctx.status = 400;
        }
    }
}