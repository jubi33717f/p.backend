const Message = require('../models/MessageModel');
const assert = require('assert');
const mongoose = require('mongoose');
const Admin = require('../models/AdminModel');
const Employee = require('../models/EmployeeModel');
//store=>post增加一条message
//destory=>delete(writerId 匹配 or writerRole='Admin' )
//update=>put(writerId 匹配 or writerRole='Admin' )
//index=>get（分页，前端计算处理实现分段效果）
//indexMe=>get one(详细的post页面)

//indexReverse=>逆序排序显示=>依然需要分页
//indexPopular=>like排序显示=>依然需要分页
//searchContent=>关键字查询
//indexTopic=>topic筛选=>依然需要分页
exports.indexMyPosts = async(ctx)=>{
    let {id} = ctx.params;

    try{
        var results = await Message.find({
            writerId:id,
        });
       


        ctx.body = {
            result:results,
            total:results.length
        }
        ctx.status = 200;
    }catch(e){
        if(e instanceof assert.AssertionError){
            ctx.body = e.message;
            ctx.status = 400;
        }
    }
}
exports.indexReverse = async(ctx)=>{
    let {page,pageSize} = ctx.params;
    // const token = ctx.request.headers['x-access-token'];
    // if (!token){
    //     ctx.body={
    //         auth: false, message: 'No token provided.' 
    //     }
    //     ctx.status = 401;
    //     return
    // } 
    page = +page;
    pageSize = +pageSize;
    var skip = (page-1) * pageSize;
    try{
        var total = await Message.find().countDocuments();
        var results = await Message.find({},{}).skip(skip).limit(pageSize);
        ctx.body = {
            result:results,
            total,
        }
        ctx.status = 200;
    }catch(e){
        if(e instanceof assert.AssertionError){
            ctx.body = e.message;
            ctx.status = 400;
        }
    }
} 
exports.searchContent = async(ctx)=>{
    let {content} = ctx.params;
    try{
        const results = await Message.find({content:{$regex:new RegExp('^.*'+content+'.*$')}})
        const total = results.length;
  
        ctx.body = {
            result:results,
            total,
        }
        ctx.status = 200;
    }catch(e){
        if(e instanceof assert.AssertionError){
            ctx.body = e.message;
            ctx.status = 400;
        }
    }
}
exports.indexPopular = async(ctx)=>{
    let {page,pageSize} = ctx.params;
    page = +page;
    pageSize = +pageSize;
    var skip = (page-1) * pageSize;
    try{
        var total = await Message.find().countDocuments();
  
    // const token = ctx.request.headers['x-access-token'];
    // if (!token){
    //     ctx.body={
    //         auth: false, message: 'No token provided.' 
    //     }
    //     ctx.status = 401;
    //     return
    // } 
        
        
        let results = await Message.find().sort({like:-1}).skip(skip).limit(pageSize);

        ctx.body = {
            result:results,
            total,
        }
        ctx.status = 200;
    }catch(e){
        if(e instanceof assert.AssertionError){
            ctx.body = e.message;
            ctx.status = 400;
        }
    }
}
exports.indexTopic = async(ctx)=>{
    let {page,pageSize,topic} = ctx.params;
    try{
        let results = await Message.find({tag:{$regex:new RegExp(topic)}})

        const total = results.length

        results  = results.reverse().slice((page-1)*pageSize, (page-1)*pageSize+pageSize)

        ctx.body = {
            result:results,
            total,
        }
        ctx.status = 200;
    }catch(e){
        if(e instanceof assert.AssertionError){
            ctx.body = e.message;
            ctx.status = 400;
        }
    }
}
exports.index = async(ctx)=>{
    let {page,pageSize} = ctx.params;
    
    try{
        var total = await Message.find().countDocuments();

    // const token = ctx.request.headers['x-access-token'];
    // if (!token){
    //     ctx.body={
    //         auth: false, message: 'No token provided.' 
    //     }
    //     ctx.status = 401;
    //     return
    // } 
        
        
        let results = await Message.find();
        results  = results.reverse().slice((page-1)*pageSize, (page-1)*pageSize+pageSize)

        ctx.body = {
            result:results,
            total,
        }
        ctx.status = 200;
    }catch(e){
        if(e instanceof assert.AssertionError){
            ctx.body = e.message;
            ctx.status = 400;
        }
    }
}
exports.indexMe = async ctx=>{
    let {id} = ctx.params;
    //const token = ctx.request.headers['x-access-token'];
    // if (!token){
    //     ctx.body={
    //         auth: false, message: 'No token provided.' 
    //     }
    //     ctx.status = 401;
    //     return
    // }
    try{
        var results = await Message.findOne({
            _id:new mongoose.Types.ObjectId(id),
        });
        
        ctx.body = {
            result:results,
        }
        ctx.status = 200;
    }catch(e){
        if(e instanceof assert.AssertionError){
            ctx.body = e.message;
            ctx.status = 400;
        }
    }
}

exports.store = async (ctx)=>{
    // const token = ctx.request.headers['x-access-token'];
    // if (!token) return res.status(401).send({ auth: false, message: 'No token provided.' });
    try{
        const {body} = ctx.request;
        assert(body.name != "","writer must exist");
        assert(body.photo != "","writer photo must exist");
        assert(body.writerId != "","writer id body must exist");
        assert(body.writerRole != "","writer role must exist");
        // assert(body.postTime != "","post time must exist");
        // assert(body.tag != "","tag must exist");
        assert(body.content != "","post content must exist");
        body.postTime=(new Date()+'').substring(0,24)
        body.like=0;
        body.dislike=0;
        const newMessage = new Message(body);
        const res = await newMessage.save();
        ctx.body={
            message:"Created",
            id:res._id
        }
        ctx.status = 201;
    }catch(e){
        if(e instanceof assert.AssertionError){
            ctx.body = e.message;
            ctx.status = 400;
        }  
    }
}
exports.update = async (ctx)=>{
    const {id} = ctx.params;
    assert(id,'id  must  exist!');
    // assert(body.writerId != "","writer id body must exist");
    // assert(body.writerRole != "","writer role must exist");

    // const token = ctx.request.headers['x-access-token'];
    // if (!token){
    //     ctx.body={
    //         auth: false, message: 'No token provided.' 
    //     }
    //     ctx.status = 401;
    //     return
    // } 
    const message =  await Message.findOne({_id:new mongoose.Types.ObjectId(id)});
    if(message.writerRole !== 'Admin' || message.writerId !== body.writerId){
        ctx.body = {
            message:`You do not have the authorization`
        };
        ctx.status = 401;
    }else{
        const {body} = ctx.request;
        body.postTime=(new Date()+'').substring(0,24)
        if(message){
            Object.assign(message,body);
            const newMessage = new Message(message);
            const res = await newMessage.save();
            ctx.body = {
                message:'update',
            }
        }else{
            ctx.body = {
                message:`${id} not found`
            };
            ctx.status = 404;
        }
    }
    
    
}
exports.updateComment = async (ctx)=>{
    const {id} = ctx.params;
    assert(id,'id  must  exist!');
    // assert(body.writerId != "","writer id body must exist");
    // assert(body.writerRole != "","writer role must exist");

    // const token = ctx.request.headers['x-access-token'];
    // if (!token){
    //     ctx.body={
    //         auth: false, message: 'No token provided.' 
    //     }
    //     ctx.status = 401;
    //     return
    // } 
    const message =  await Message.findOne({_id:new mongoose.Types.ObjectId(id)});
    
    const {body} = ctx.request;
    
    if(message){
        Object.assign(message,body);
        const newMessage = new Message(message);
        const res = await newMessage.save();
        ctx.body = {
            message:'update',
        }
    }else{
        ctx.body = {
            message:`${id} not found`
        };
        ctx.status = 404;
    }
    
    
    
}

exports.updateLike = async(ctx)=>{
    const {id} = ctx.params;
    assert(id,'id  must  exist!');
    const {body} = ctx.request;
    assert(body.id != "","writer must exist");
    const message =  await Message.findOne({_id:new mongoose.Types.ObjectId(id)});

    if(JSON.stringify(message.likePeople)==='[null]'||message.likePeople.indexOf(body.id) === -1){
        let newMessage = message;
        newMessage.like = message.like+1
        newMessage.likePeople.push(body.id)
        Object.assign(message,newMessage);
        const nMessage = new Message(newMessage);
        const res = await nMessage.save();
        ctx.body = {
            message:'update',
        }

    }else{
        ctx.body = {
            message:`${id} not found`
        };
        ctx.status = 404;
    }
}
exports.updateDislike = async(ctx)=>{
    const {id} = ctx.params;
    assert(id,'id  must  exist!');
    const {body} = ctx.request;
    assert(body.id != "","writer must exist");
    const message =  await Message.findOne({_id:new mongoose.Types.ObjectId(id)});
    if(JSON.stringify(message.dislikePeople)==='[null]'||message.dislikePeople.indexOf(body.id) === -1){
        let newMessage = message;
        newMessage.dislike = message.dislike+1
        newMessage.dislikePeople.push(body.id)
        Object.assign(message,newMessage);
        const nMessage = new Message(newMessage);
        const res = await nMessage.save();
        ctx.body = {
            message:'update',
        }

    }else{
        ctx.body = {
            message:`${id} not found`
        };
        ctx.status = 404;
    }
}
exports.destroy = async (ctx)=>{
    const { id } = ctx.params;
    assert(id, "id must exist!");
    const token = ctx.request.headers['x-access-token'];
    if (!token){
        ctx.body={
            auth: false, message: 'No token provided.' 
        }
        ctx.status = 401;
        return
    } 
    
    const res = await Message.deleteOne({
        _id: new mongoose.Types.ObjectId(id),
    });
    if(message.writerRole !== 'Admin' || message.writerId !== body.writerId){
        ctx.body = {
            message:`You do not have the authorization`
        };
        ctx.status = 401;
    }else{
        ctx.body = res;
    }   
}
exports.indexUnreadMessage = async (ctx)=>{
    let {id,role} = ctx.params;
    let res1;
    if(role === 'Admin'){
        res1=await Admin.findOne({_id:new mongoose.Types.ObjectId(id)})
    }else{
        res1=await Employee.findOne({_id:new mongoose.Types.ObjectId(id)})
    }
    if(res1){
        
        let res2 = await Message.find()
        if(res2){
            let res = res2.filter(ele=>new Date(ele.postTime)>new Date(res1.messageReadTime))
            ctx.body={
                unreadMessageAmount:res.length
            }
            ctx.status = 200;
        }
        
    }

}