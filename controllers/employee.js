const Employee = require('../models/EmployeeModel');
// const jwt = require('jsonwebtoken');
// const bcrypt = require('bcryptjs');
const assert = require('assert');
const mongoose = require('mongoose');
//const config = require('../config');
exports.store = async (ctx)=>{
    try{
        const {body} = ctx.request.body;
        const {account ,firstname,department,role} = body;
        //account用邮箱(现实场景用单位邮箱)
        assert(account !== undefined,'account must exist');
        assert(firstname !== undefined,'firstname must exist');
        //后台如何自动分配密码？
        //body.password=account默认
        body.password  = account;
        const employee = new Employee(body);
        
        const res=await employee.save()

        const id= JSON.stringify(res._id).slice(-5,-1) ;
         //body.employeeID 
         body.leaveRecord = [...Array(31)].map(_=>-2);
        switch(department){
            case 'Web Development': {
                body.employeeID='WDLP'+id;
                if(role === 'head'){body.salary = 10000;} 
                else{body.salary = 5000;} 
                break;
            }
                
            case 'Marketing':{
                body.employeeID='MKT'+id;
                if(role === 'head'){body.salary = 8000;} 
                else{body.salary = 4000;} 
                break;
                }
            case 'App Development':{
                body.employeeID='ADLP'+id;
                if(role === 'head'){body.salary = 10000;} 
                else{body.salary = 5000;} 
                break;
            }
            case 'Support':{
                body.employeeID='SP'+id;
                if(role === 'head'){body.salary = 6000;} 
                else{body.salary = 3000;} 
                break;
                }
            case 'Accounts':{
                body.employeeID='ACCT'+id;
                if(role === 'head'){body.salary = 8000;} 
                else{body.salary = 4000;} 
                break;
                }
            default:body.employeeID='EM'+id;
        }
    
        const employee_ = await Employee.findOne({
            _id: res._id
        });
        Object.assign(employee_, body);
        const newEmployee = new Employee(employee_);
        const res_ = await newEmployee.save();
        
        ctx.body={
            message:'created',
            id:res._id,
            
        }
        ctx.status = 201;
    }catch(e){
        if(e instanceof assert.AssertionError){
            ctx.body=e.message;
            ctx.status=400;
        }else{
            ctx.body=e.message;
        }
    }
}
exports.index = async(ctx)=>{
    let {page,pageSize} = ctx.params;
    // const token = ctx.request.headers['x-access-token'];
    // if (!token){
    //     ctx.body={
    //         auth: false, message: 'No token provided.' 
    //     }
    //     ctx.status = 401;
    //     return
    // } 
    page = +page;
    pageSize = +pageSize;
    const skip = (page-1) * pageSize;
    const total = await Employee.find().countDocuments();
    const res = await Employee.find({},{}).skip(skip).limit(pageSize);
    ctx.body = {
        results:res,
        total,
    };
}
exports.indexEmployee = async ctx=>{
    let {name} = ctx.params;
    // const token = ctx.request.headers['x-access-token'];
    // if (!token){
    //     ctx.body={
    //         auth: false, message: 'No token provided.' 
    //     }
    //     ctx.status = 401;
    //     return
    // }
    try{
        var results = await Employee.find({$or:[{firstname:name},{lastname:name}]});
        ctx.body = {
            result:results,
        }
        ctx.status = 200;
    }catch(e){
        if(e instanceof assert.AssertionError){
            ctx.body = e.message;
            ctx.status = 400;
        }
    }
}

exports.show = async(ctx)=>{
    // return '1'
    const {id} = ctx.params;
    assert(id,'id  must  exist!');
    const token = ctx.request.headers['x-access-token'];
    if (!token){
        ctx.body={
            auth: false, message: 'No token provided.' 
        }
        ctx.status = 401;
        return
    } 

    const  res = await Employee.findOne({
        _id:new mongoose.Types.ObjectId(id),
    });

    if(res){
        ctx.body=res;
    }else{

        ctx.body={
            message:`${id}  not found`,
        }
        ctx.status = 404;
    }
}
exports.update = async (ctx)=>{
    const {id} = ctx.params;
    assert(id,'id  must  exist!');

    // const token = ctx.request.headers['x-access-token'];
    // if (!token){
    //     ctx.body={
    //         auth: false, message: 'No token provided.' 
    //     }
    //     ctx.status = 401;
    //     return
    // } 
    // jwt.verify(token, config.secret, function(err, decoded) {
    //     if (err){
    //         ctx.body={
    //             auth: false, message: 'Failed to authenticate token.'
    //         }
    //         ctx.status = 500;
    //         return
    //     } });
    const employee =  await Employee.findOne({_id:new mongoose.Types.ObjectId(id)});
    const {body} = ctx.request;

    if(employee){
        Object.assign(employee,body);
        const newEmployee = new Employee(employee);
      
        
        const res = await newEmployee.save();
        ctx.body = {
            message:'update',
        }
    }else{
        ctx.body = {
            message:`${id} not found`
        };
        ctx.status = 404;
    }
    
}
exports.destroy = async (ctx)=>{
    const { id } = ctx.params;
    assert(id, "id must exist!");
    const token = ctx.request.headers['x-access-token'];
    if (!token){
        ctx.body={
            auth: false, message: 'No token provided.' 
        }
        ctx.status = 401;
        return
    } 
    
    const res = await Employee.deleteOne({
        _id: new mongoose.Types.ObjectId(id),
    });

    ctx.body = res;
}
exports.salaryUpdate = async (ctx)=>{
    // const token = ctx.request.headers['x-access-token'];
    // if (!token){
    //     ctx.body={
    //         auth: false, message: 'No token provided.' 
    //     }
    //     ctx.status = 401;
    //     return
    // } 
    // const employee =  await Employee.find({_id:new mongoose.Types.ObjectId(id)});
    const employees =  await Employee.find();


    employees.forEach(async function (item,index){
        let id = item._id;
        let employee =  await Employee.find({_id:new mongoose.Types.ObjectId(id)});
        
        if(employee){
            let records = employee[0].leaveRecord;
            let numWork = (records.filter(record => record===0)).length;
            if (numWork >= 20){
                employee[0].bonus = 200;
            }
            employee[0].earnings = employee[0].bonus + employee[0].salary;
            employee[0].tax =  employee[0].salary * 0.1;
            let numLeave = (records.filter(record => record===0.5)).length * 0.5 + 
            (records.filter(record => record===-1)).length +
            (records.filter(record => record===-0.5)).length * 0.75;
            employee[0].unpaidLeave = parseInt(employee[0].salary /28 *numLeave);
            employee[0].deduction = employee[0].unpaidLeave+employee[0].tax;

            
            const newEmployee = new Employee(employee[0]);
            const res = await newEmployee.save();
        }else{
            ctx.body = {
                message:`${id} not found`
            };
            ctx.status = 404;
            throw id;
        }
    })
    ctx.body = {
        message:'update',
    }
}
exports.indexSort=async ctx=>{
    // const token = ctx.request.headers['x-access-token'];
    // if (!token) return res.status(401).send({ auth: false, message: 'No token provided.' });
    try{
        const {page,pageSize,field} = ctx.params;
        assert(page,"page must exist");
        assert(pageSize,"pageSize must exist");
        assert(field,"field must exist");
        const attrArray = ["photo","employeeID","firstname","mobile","startDate","role"];
        assert(attrArray.includes(field),"field should match the attribute");
        const pageNum = parseInt(page);
        const pageSizeNum = parseInt(pageSize);
        
        const skipNum = (pageNum-1)*pageSizeNum;
        const totalNum = await Employee.find().countDocuments();
        const result = await Employee.find().sort([[field, 1]]).skip(skipNum).limit(pageSizeNum);
        ctx.body = {
            result:result,
            total:totalNum
        }
        ctx.status = 200;
    }catch(e){
        if(e instanceof assert.AssertionError){
            ctx.body = e.message;
            ctx.status = 400;
        }
    }
}