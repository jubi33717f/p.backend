const Leave = require('../models/LeaveModel');
const Employee = require('../models/EmployeeModel');
const assert = require('assert');
const mongoose = require('mongoose');


exports.index = async(ctx)=>{
    let {page,pageSize} = ctx.params;
    const token = ctx.request.headers['x-access-token'];
    if (!token){
        ctx.body={
            auth: false, message: 'No token provided.' 
        }
        ctx.status = 401;
        return
    } 
    page = +page;
    pageSize = +pageSize;
    var skip = (page-1) * pageSize;
    try{
        var total = await Leave.find().countDocuments();
        var results = await Leave.find({},{}).skip(skip).limit(pageSize);
        ctx.body = {
            result:results,
            total,
        }
        ctx.status = 200;
    }catch(e){
        if(e instanceof assert.AssertionError){
            ctx.body = e.message;
            ctx.status = 400;
        }
    }
}
exports.indexLeave = async ctx=>{
    let {name} = ctx.params;
    const token = ctx.request.headers['x-access-token'];
    if (!token){
        ctx.body={
            auth: false, message: 'No token provided.' 
        }
        ctx.status = 401;
        return
    }
    try{
        var results = await Leave.find({name:name});
        ctx.body = {
            result:results,
        }
        ctx.status = 200;
    }catch(e){
        if(e instanceof assert.AssertionError){
            ctx.body = e.message;
            ctx.status = 400;
        }
    }
}

exports.store = async (ctx)=>{
    try{
        const {body} = ctx.request
        const newLeave = new Leave(body);
        const res = await newLeave.save();
        var putEm = await Employee.find({"employeeID":body.employeeID});
        var arr = putEm[0].leaveRequest            
        arr.name = body.name
        arr.employeeID = body.employeeID
        arr.dateStart = body.dateStart
        arr.dateEnd = body.dateEnd
        arr.leaveType= body.leaveType
        arr.reason  = body.reason
        arr.status = '0'
        re = await Employee.updateOne({"employeeID":body.employeeID}, {$set:{leaveRequest: arr}});
        ctx.body={
            message:"Created",
            id:res._id
        }
        ctx.status = 201;
    }catch(e){
        if(e instanceof assert.AssertionError){
            ctx.body = e.message;
            ctx.status = 400;
        }  
    }
}
exports.destroy =async ctx=>{
    const token = ctx.request.headers['x-access-token'];
    if (!token){
        ctx.body={
            auth: false, message: 'No token provided.' 
        }
        ctx.status = 401;
        return
    } 
    try{
        const {id,dateStart,dateEnd,leaveType, flag} = ctx.params;
        assert(id,"id must exist");
        var t = 1
        if (leaveType === 'Half-Day') {
            t = 0.5
        }
        var res = await Leave.deleteOne({"employeeID":id});
        if (flag === '1'){
            if (t === 0.5) {
                let index_day = Number(dateStart.split('-')[2]) - 1       
                var putEm = await Employee.find({"employeeID":id});
                var arr = putEm[0].leaveRecord
                var a = putEm[0].leaveRequest
                a.status = '1'
                arr[index_day] = t
                res = await Employee.updateOne({"employeeID":id}, {$set:{leaveRecord: arr}});
                rs = await Employee.updateOne({"employeeID":id}, {$set:{leaveRequest: a}});
                ctx.body = {
                    result:res,
                }
                ctx.status = 200;
            } else {
                let index_s = Number(dateStart.split('-')[2])
                let index_e = Number(dateEnd.split('-')[2])
                let num = index_e - index_s + 1
                const putEm = await Employee.find({"employeeID":id});
                var arr = putEm[0].leaveRecord
                var a = putEm[0].leaveRequest
                a.status = '1'
                for (let i = 0; i < num; i++){
                    let x = index_s + i - 1
                    arr[x] = t          
                }
                res = await Employee.updateOne({"employeeID":id}, {$set:{leaveRecord: arr}});
                rs = await Employee.updateOne({"employeeID":id}, {$set:{leaveRequest: a}});
                ctx.body = {
                    result:res,
                }
                ctx.status = 200;
            }
        } else {
            const putEm = await Employee.find({"employeeID":id});
            var a = putEm[0].leaveRequest
            a.status = '2'
            rs = await Employee.updateOne({"employeeID":id}, {$set:{leaveRequest: a}});
            ctx.body = res;
            ctx.status = 200;
        }       
    }catch(e){
        if(e instanceof assert.AssertionError){
            ctx.body = e.message;
            ctx.status = 400;
        }
    }
}