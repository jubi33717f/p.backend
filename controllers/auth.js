const Admin = require('../models/AdminModel');
const Employee = require('../models/EmployeeModel');
const mongoose = require('mongoose');
const jwt = require('jsonwebtoken');
const bcrypt = require('bcryptjs');
const assert = require('assert');
const config = require('../config');
exports.login = async (ctx) => {
    const { account, password } = ctx.request.body;
    //console.log(ctx.request.body)
    const res1=await Admin.findOne({ account, password })
    const res2=await Employee.findOne({ account, password })
    if (res1) {
      let token = jwt.sign({ id: res1._id }, config.secret, {
            expiresIn: 86400 // expires in 24 hours
      });
    //   console.log(res1)
      //ctx.body = res1;
      ctx.body={
        message:'success',
        id:res1._id,
        auth:'Admin',
        token:token,
        firstname:res1.firstname,
        lastname:res1.lastname,
        gender:res1.gender,
        photo:res1.photo,
        experience:res1.experience,
        clients:res1.clients,
        employee:res1.employee
      }
      ctx.status = 200;
    } else if(res2){
        let token = jwt.sign({ id: res2._id }, config.secret, {
            expiresIn: 86400 // expires in 24 hours
        });
        //ctx.body = "Employee log in success";
         
        ctx.body={
            message:'success',
            id:res2._id,
            auth:'Employee',
            token:token,
            firstname:res2.firstname,
            lastname:res2.lastname,
            gender:res2.gender,
            photo:res2.photo,
            role:res2.role,
            department:res2.department,
            emId:res2.employeeID,
            account:res2.account,
        }
        ctx.status = 200;
    }else {
      ctx.body = "Account or password is not correct.";
      ctx.status = 404;
    }
}
exports.changepassword=async (ctx)=>{
    const {account,old_password,new_password} = ctx.request.body;
    assert(account,'account  must  exist!');
    assert(old_password,'password  must  exist!');
    
    //console.log(id)
    const admin=await Admin.findOne({ account, password:old_password })
    const employee=await Employee.findOne({ account, password:old_password })
    const update = {password:new_password}
    
    if(admin){
        Object.assign(admin,update);
        const newAdmin= new Admin(admin);
        const res = await newAdmin.save();
        ctx.body = {
            role:'admin',
            message:'update',
        }
        
    }else if(employee){
        Object.assign(employee,update);
        const newEmployee = new Employee(employee);
        const res = await newEmployee.save();
        ctx.body = {
            role:'employee',
            message:'update',
        }
    }else{

        ctx.body={
            message:`not found`,
        }
        ctx.status = 404;
    }
}