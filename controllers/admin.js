const Admin = require('../models/AdminModel');
const mongoose = require('mongoose');
//const config = require('../config');
// const jwt = require('jsonwebtoken');
// const bcrypt = require('bcryptjs');
const assert = require('assert');
exports.store = async (ctx)=>{
    try{
        const {body} = ctx.request;
        const {account,firstname}=body;
        assert(account !== undefined,'account must exist');
        assert(firstname !== undefined,'firstname must exist');
        
        body.password  = account;
        const admin = new Admin(body);      
        const res=await admin.save();
        ctx.body={
            message:'created',
            id:res._id,
        }
        ctx.status = 201;
    }catch(e){
        if(e instanceof assert.AssertionError){
            ctx.body=e.message;
            ctx.status=400;
        }else{
            ctx.body=e.message;
        }
    }
}
exports.index = async(ctx)=>{
    let {page,pageSize} = ctx.params;
    const token = ctx.request.headers['x-access-token'];
    if (!token){
        ctx.body={
            auth: false, message: 'No token provided.' 
        }
        ctx.status = 401;
        return
    } 
    page = +page;
    pageSize = +pageSize;

    const skip = (page-1) * pageSize;
    const total = await Admin.find().countDocuments();
    const res = await Admin.find({},{}).skip(skip).limit(pageSize);
    ctx.body = {
        results:res,
        total,
    };
}
exports.show = async(ctx)=>{
    const {id} = ctx.params;
    assert(id,'id  must  exist!');
    const token = ctx.request.headers['x-access-token'];
    if (!token){
        ctx.body={
            auth: false, message: 'No token provided.' 
        }
        ctx.status = 401;
        return
    } 
    
    const  res = await Admin.findOne({
        _id:new mongoose.Types.ObjectId(id),
    });

    if(res){
        ctx.body=res;
    }else{

        ctx.body={
            message:`${id}  not found`,
        }
        ctx.status = 404;
    }
}
exports.update = async (ctx)=>{
    const {id} = ctx.params;
    assert(id,'id  must  exist!');
    // const token = ctx.request.headers['x-access-token'];
    // if (!token){
    //     ctx.body={
    //         auth: false, message: 'No token provided.' 
    //     }
    //     ctx.status = 401;
    //     return
    // } 
    const  admin =  await Admin.findOne({_id:new mongoose.Types.ObjectId(id)});
    const {body} = ctx.request;
    if(admin){
        Object.assign(admin,body);
        const newAdmin = new Admin(admin);
        const res = await newAdmin.save();
        ctx.body = {
            message:'update',
        }
    }else{
        ctx.body = {
            message:`${id} not found`
        };
        ctx.status = 404;
    }
    
}
exports.destroy = async (ctx)=>{
    const { id } = ctx.params;
    assert(id, "id must exist!");
    const token = ctx.request.headers['x-access-token'];
    if (!token){
        ctx.body={
            auth: false, message: 'No token provided.' 
        }
        ctx.status = 401;
        return
    } 
    const res = await Admin.deleteOne({
        _id: new mongoose.Types.ObjectId(id),
    });

    ctx.body = res;
}
